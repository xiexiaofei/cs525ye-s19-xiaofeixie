Added Structures:
For implementation, we added extra structures(Object in JAVA) to buffer_mgr.h.
Following are the added structure lists:
1. BM_PageFrame is a structure for storing information of a page frame:
typedef struct BM_PageFrame

2. BM_RepHandle is about replacement handle: the cur is the position for Clock and FIFO, pointing to the page
frame that's going to be replaced next; the size is the total number of page frames in Buffer Manager. 
typedef struct BM_RepHandle{//replacement handle
	int cur,size;
	void *stratData;//passed in from initBufferPool
	void *dataStruct;//structure to help with the strategy
}BM_RepHandle;

3. BM_MgData is use to handle pageframes ot buffer pool:
typedef struct BM_MgData

4. ListEntry is use to define the link list and list entries like below:
typedef struct ListEntry{
	int index;//the pageframe index
	struct ListEntry *p;
	struct ListEntry *n;
}ListEntry;

5. IntList is use to define the IntList like below:
typedef struct IntList{
	ListEntry *h,*t;//dequeue from head, enqueue from tail
}IntList;

6. LRU is use to define the frame of IntList and ListEntry:
typedef struct LRU{
	IntList *frameList;//a list of page frame numbers
	ListEntry *frameMap;//map from page frame number to ListEntry
}LRU;


================================================================================================================


Functions lists:
1. Buffer Pool Functions
These functions are used to create a buffer pool for an existing page file (initBufferPool), shutdown a buffer 
pool and free up all associated resources (shutdownBufferPool), and to force the buffer manager to write all 
dirty pages to disk (forceFlushPool).
- 1). initBufferPool(): 
        It creates a new buffer pool��
        The numPages variable defines number of page frames; 
        The strategy method help to take page replacement;
        The pageFileName memorizes all pages' names from the page file;
        The stratData be used to pass parameters for the page replacement strategy.
- 2). shutdownBufferPool(): 
         It destroys a buffer pool;
         It should free up all resources associated with buffer pool;
         It should written back the dirty pages to disk before destroying the pool.
- 3). forceFlushPool(): 
         It force the buffer manager to write all dirty pages to disk.

2. Page Management Functions 
These functions are used pin pages, unpin pages, mark pages as dirty, and force a page back to disk.
- 1). pinPage(): 
         It pins the page with page number variable pageNum;
         Point the data field to the page frame the page is stored in.
- 2). unpinPage():
         Unpins the page variable page;
         The pageNum field of variable page use to figure out which page to unpin. 
- 3). markDirty(): 
         Marks a page as dirty.
- 4). forcePage(): 
         Write the current content of the page back to the page file on disk.

3. Statistics Functions
- 1). getFrameContents(): 
         It returns an array of PageNumbers where the ith element is the number of the page stored in the ith 
          page frame;
         Empty page frame is represented using the constant NO_PAGE;         
- 2). getDirtyFlags(): 
         It returns an array of bools where the ith element is TRUE if the page stored in the ith page frame is dirty;
         Empty page frames are considered as clean.
- 3). getFixCounts(): 
         It returns an array of ints where the ith element is the fix count of the page stored in the ith page frame;
         Return 0 for empty page frames.
- 4). getNumReadIO(): 
         It returns the number of pages that have been read from disk since a buffer pool has been initialized;
         It initialize this statistic at pool creating time and update whenever a page is read from the page file into 
          a page file into a page frame.
- 5). getNumWriteIO(): 
         It returns the number of pages written to the page file since the buffer pool has been initialized.




