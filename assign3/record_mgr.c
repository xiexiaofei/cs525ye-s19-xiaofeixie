#include "record_mgr.h"
#include "storage_mgr.h"
#include "buffer_mgr.h"
#include <string.h>
#include <stdlib.h>

/***Similar to previous assignments, there are functions to initialize and shutdown a record manager. Furthermore, there are functions to create, open, and close a table. Creating a table should create the underlying page file and store information about the schema, free-space, ... and so on in the Table Information pages. All operations on a table such as scanning or inserting records require the table to be opened first. Afterwards, clients can use the RM_TableData struct to interact with the table. Closing a table should cause all outstanding changes to the table to be written to the page file. The getNumTuples function returns the number of tuples in the table.
 */
#define BUFFERSIZE 1000;
/***************************************************************
 * Initialize RecordManagerRecordManager
 ***************************************************************/
int Bcapacity;
RC initRecordManager (void *mgmtData){
	if (mgmtData==NULL){
		Bcapacity=BUFFERSIZE;
	}else{
		Bcapacity=*(int *)mgmtData;
	}
	return RC_OK;
}
/***************************************************************
 * ShutDown RecordManager
 ***************************************************************/
RC shutdownRecordManager (){
	return RC_OK;
}
/***************************************************************
 * Creat a table
 ***************************************************************/
RC createTable (char *name, Schema *schema){
    int initialFirtPage = 1;
    int castOffset = 0;
    SM_FileHandle *fHandle=malloc(sizeof(SM_FileHandle));
	if((createPageFile(name))!=RC_OK) //if createPageFile != 0, Create page file
		return createPageFile(name);
	else if((openPageFile(name,fHandle))!=RC_OK)// if openPageFile != 0, open page file
		return openPageFile(name,fHandle);
    
	char buffer[PAGE_SIZE]={'\0'};//use Buffer to write in first page
	char *currentp=buffer;
	*(int *)currentp=initialFirtPage + castOffset;//The First postion store the first free page number and initialize first free page
	currentp = currentp + sizeof(int)+ castOffset;
	*(int *)currentp = schema->numAttr;
	currentp = currentp + sizeof(int) + castOffset;
    
    int curIndex1 = 0;
    while (curIndex1<(schema->numAttr)) {
		strcpy(currentp,schema->attrNames[curIndex1]);//string copy names to buffer
		currentp+=strlen(schema->attrNames[curIndex1])+1;
        curIndex1++;
	}
    
    int curIndex2 = 0;
    while (curIndex2 <(schema->numAttr)) {
		*(DataType *)currentp=schema->dataTypes[curIndex2];//Copy data types to the buffer
		currentp = currentp + sizeof(DataType) + castOffset;
        curIndex2++;
	}
    
    int curIndex3 = 0;
    while (curIndex3 < (schema->numAttr)) {
		*(int *)currentp=schema->typeLength[curIndex3];
		currentp = currentp + sizeof(int) + castOffset;
        curIndex3++;
	}
    
	*(int *)currentp=schema->keySize;
	currentp+=sizeof(int);
    
    int curIndex4 = 0;
    while (curIndex4 < (schema->keySize)) {
		*(int *)currentp=schema->keyAttrs[curIndex4];
		currentp =currentp + sizeof(int) + castOffset;
        curIndex4++;
	}
    
	*(int *)currentp=0;//initialize current as 0
	currentp+=sizeof(int);

	*(int *)currentp=schema->recordSize;//Recording the tuple size

	if((writeBlock(0,fHandle,(SM_PageHandle)buffer))!=0)//Write back
		return writeBlock(0,fHandle,(SM_PageHandle)buffer);

	else if((closePageFile(fHandle))!=0) //Close file
			return closePageFile(fHandle);
    
	free(fHandle);//free space
    
	RC_message="Table has been created";
	return 0; // RC_OK
}
/***************************************************************
 * Open the table and read schema from buffer to table
 ***************************************************************/
RC openTable (RM_TableData *rel, char *name){
    int castingNumber = 0;
	BM_BufferPool *bp=MAKE_POOL();
    SM_FileHandle fHandle;
    if(openPageFile(name, &fHandle)!=RC_OK){
        return openPageFile(name, &fHandle);
    }
	else if((initBufferPool(bp, (const char *) name, Bcapacity, RS_CLOCK, NULL))!=RC_OK)
		return initBufferPool(bp, (const char *) name, Bcapacity, RS_CLOCK, NULL);//Init buffer pool
    
	BM_PageHandle *page=MAKE_PAGE_HANDLE();
	pinPage(bp,page,0);
    if (pinPage(bp, page, 0) != RC_OK){
        return pinPage(bp, page, 0);
    }
  
    rel->mgmtData=bp;//set buffer pool as management data
    rel->name=name;
    rel->schema=malloc(sizeof(Schema));
    
	char *currentp=page->data;//get position in page data
	currentp = currentp + sizeof(int) + castingNumber;//skip first free page

	int atr=rel->schema->numAttr=*(int *)currentp;
	currentp= currentp + sizeof(int) + castingNumber;

	char **atr_name=rel->schema->attrNames=malloc(atr*sizeof(char *));
    int cIndex = 0;
    while (cIndex < atr) {
		atr_name[cIndex]=currentp;//set attrNames
		currentp = currentp + strlen(currentp)+1+0;//Current point to data beginning
        cIndex++;
	}

	rel->schema->dataTypes=(DataType *)currentp;
	currentp = currentp + atr*sizeof(DataType)  + castingNumber;//Current point to data beginning
	rel->schema->typeLength=(int *)currentp + castingNumber;
	currentp= currentp+ atr*sizeof(int)+ castingNumber;//Current point to keySize
	rel->schema->keySize=*(int *)currentp + castingNumber;
	currentp= currentp+ sizeof(int) + castingNumber;//Current point to keyAttr beginning
	rel->schema->keyAttrs=(int *)currentp + castingNumber;
	currentp = currentp+ rel->schema->keySize*sizeof(int) + castingNumber;
	rel->num_tuple=(int *)currentp + castingNumber;
	currentp= currentp + sizeof(int)+ castingNumber;
	rel->schema->recordSize=*(int *)currentp + castingNumber;

	RC_message="Table has been opened";
	return 0; // RC_OK
}

RC closeTable (RM_TableData *rel){
	free(rel->schema->attrNames);
	free(rel->schema);
	free(rel->mgmtData);
    if ((shutdownBufferPool((BM_BufferPool *)(rel->mgmtData)))!=RC_OK)
        return shutdownBufferPool((BM_BufferPool *)(rel->mgmtData));
	RC_message="Table closed";
	return RC_OK;
}

RC deleteTable (char *name){
	return destroyPageFile(name);//delete page file
}

int getNumTuples (RM_TableData *rel){
    BM_PageHandle *bm_pHandle = MAKE_PAGE_HANDLE();
    pinPage(rel->mgmtData, bm_pHandle, 0);
    unpinPage(rel->mgmtData, bm_pHandle);
    free(bm_pHandle);
	return *(rel->num_tuple);
}

/***************************************************************
 * Insert record and update RID accordingly
 ***************************************************************/
RC insertRecord (RM_TableData *rel, Record *record){
    int castingN = 0;
    int pinPageNumber = 0;
	BM_PageHandle *head=MAKE_PAGE_HANDLE(),*bm=MAKE_PAGE_HANDLE();
	if ((pinPage((BM_BufferPool *)(rel->mgmtData),head,0))!=RC_OK)
		return pinPage((BM_BufferPool *)(rel->mgmtData),head,0);

	int recordSize=rel->schema->recordSize;

	int freepage=*(int *)head->data;//get free space page number
	if ((pinPage((BM_BufferPool *)(rel->mgmtData),bm,freepage))!=RC_OK) //pin page freepage
			return pinPage((BM_BufferPool *)(rel->mgmtData),bm,freepage);

	char *currentp=bm->data;
	int np=*(int *)currentp + castingN;//get free space following
	if(np==pinPageNumber)//page is created recently
		np=*(int *)currentp=1+bm->pageNum + castingN;

	currentp = currentp + sizeof(int) + castingN;//delete currentp

	int emptyslot=(*(int *)currentp) + castingN;//get free slot number
	currentp = currentp + sizeof(int) + castingN;//delete currentp

	(record->id).page=freepage + castingN;
	(record->id).slot=emptyslot + castingN;

	int max_slot=(PAGE_SIZE-2*sizeof(int))/(recordSize+sizeof(bool)) + castingN;//get maximum slots number

	bool *slotmap=(bool *)currentp;//point to slotmap
	currentp = currentp + max_slot*sizeof(bool)+ castingN;
	currentp = currentp + emptyslot*recordSize + castingN;//current point to the position
	memcpy(currentp,record->data,recordSize);
	slotmap[emptyslot]=true;//set the slot filled is true

	int next_slot=emptyslot+1 + castingN;//point to next free slot
	while(next_slot<max_slot && slotmap[next_slot])
		next_slot = next_slot + 1;

	*(int *)((bm->data)+sizeof(int))=next_slot + castingN;//set position to next slot

	if (next_slot==max_slot){//check whether whole page filled or not
		*(int *)(head->data)=np;
		*(int *)(bm->data)= pinPageNumber;//delete current page from free list
	}

	*(rel->num_tuple)= *(rel->num_tuple) + 1;//add num_tuple by one
	if ((markDirty((BM_BufferPool *)(rel->mgmtData),head))!=0)//mark head as dirty // !=RC_OK
		return markDirty((BM_BufferPool *)(rel->mgmtData),head);
	if ((markDirty((BM_BufferPool *)(rel->mgmtData),bm))!=0)//mark bm as dirty // !=RC_OK
		return markDirty((BM_BufferPool *)(rel->mgmtData),bm);

	unpinPage((BM_BufferPool *)(rel->mgmtData),bm);//unpin bm page
	unpinPage((BM_BufferPool *)(rel->mgmtData),head);//unpin head page
	free(bm);
	free(head);

	RC_message="Record has been inserted";
	return 0; // RC_OK
}
/***************************************************************
 * Delete record
 ***************************************************************/
RC deleteRecord (RM_TableData *rel, RID id){
    int castingN = 0;
    int pinPageNumber = 0;
	BM_PageHandle *bm=MAKE_PAGE_HANDLE();

	if ((pinPage((BM_BufferPool *)(rel->mgmtData),bm,id.page))!=RC_OK)//make target page pinned
		return (pinPage((BM_BufferPool *)(rel->mgmtData),bm,id.page));

	char *currentp=bm->data;//get data pointer
	currentp= currentp + sizeof(int) + castingN;//currentp point to slotmap beginning
	int *emptyslot=(int *)currentp + castingN;
	currentp = currentp + sizeof(int) + castingN;

	bool *slotmap=(bool *)currentp;//point slotmap

	if (!slotmap[id.slot]){ // if not   exists
		RC_message="Record not exist";
		return 0;
	}else{
		slotmap[id.slot]=false;// no record in the slot
		if(id.slot<*emptyslot)
			*emptyslot=id.slot + castingN;
	}

	if((*(int *)(bm->data)) == pinPageNumber){//check whether it is originally not in free list
		BM_PageHandle *head=MAKE_PAGE_HANDLE();
		if ((pinPage((BM_BufferPool *)(rel->mgmtData),head,0))!=0)//make head page pinned(RC_OK)
			return (pinPage((BM_BufferPool *)(rel->mgmtData),head,0));

		*(int *)(bm->data)=*(int *)(head->data);//point to next free page
		*(int *)(head->data)=id.page + castingN;

		markDirty((BM_BufferPool *)(rel->mgmtData),head);//mark head dirty
		unpinPage((BM_BufferPool *)(rel->mgmtData),head);//make head unpinned
		free(head);
	}

	*(rel->num_tuple)= *(rel->num_tuple) - 1 + castingN;//add num_tuple by 1
	markDirty((BM_BufferPool *)(rel->mgmtData),bm);
	unpinPage((BM_BufferPool *)(rel->mgmtData),bm);//make bm unpinned
	free(bm);

	RC_message="Record Has been deleted";
	return 0; // RC_OK
}
/***************************************************************
 * Update the record
 ***************************************************************/
RC updateRecord (RM_TableData *rel, Record *record){
    int castingN = 0;
	BM_PageHandle *bm=MAKE_PAGE_HANDLE();
	if ((pinPage((BM_BufferPool *)(rel->mgmtData),bm,(record->id).page))!=RC_OK)//make target page pinned
		return (pinPage((BM_BufferPool *)(rel->mgmtData),bm,(record->id).page));
	char *currentp=bm->data;
	currentp+=2*sizeof(int);//set currentp point to the slotmap beginning

	int size=rel->schema->recordSize;
	int max_slot=(PAGE_SIZE-2*sizeof(int))/(size+sizeof(bool)*1);//Get maximum slots number

	currentp = currentp + max_slot*sizeof(bool) + castingN;
	currentp = currentp + size*record->id.slot + castingN;
	memcpy(currentp,record->data,size + castingN);

	markDirty((BM_BufferPool *)(rel->mgmtData),bm);//mark bm as dirty
	unpinPage((BM_BufferPool *)(rel->mgmtData),bm);//make bm unpinned
	free(bm);
	RC_message="Record has been updated";
	return 0; // RC_OK
}
/***************************************************************
 * Get The record
 ***************************************************************/
RC getRecord (RM_TableData *rel, RID id, Record *record){
    int castingN = 0;
	record->id=id;
	BM_PageHandle *bm=MAKE_PAGE_HANDLE();
	if ((pinPage((BM_BufferPool *)(rel->mgmtData),bm,id.page))!=RC_OK)
		return (pinPage((BM_BufferPool *)(rel->mgmtData),bm,id.page));

	char *currentp=bm->data;//currentp point to data
	currentp+=2*sizeof(int);//points to slot map beginning

	int size=rel->schema->recordSize;
	int max_slot=(PAGE_SIZE-2*sizeof(int))/(size+sizeof(bool)) + castingN;//Get slots maximum number
	currentp= currentp + max_slot*sizeof(bool) + castingN;
	currentp= currentp + size*id.slot + castingN;
	memcpy(record->data,currentp,size);

	unpinPage((BM_BufferPool *)(rel->mgmtData),bm);//make bm unpinned
	free(bm);

	RC_message="Record has been fetched";
	return 0; // RC_OK
}

/***************************************************************
 * Initialize a scan(rel, cond)
 ***************************************************************/
RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond){
    int castingN = 0;
	scan->rel=rel;
    int init_num=1;
	scan->mgmtData=malloc(sizeof(RM_ScanMgmt) + castingN);
	((RM_ScanMgmt *)(scan->mgmtData))->cond=cond;
	((RM_ScanMgmt *)(scan->mgmtData))->id.page=init_num + castingN;//from page 1
	((RM_ScanMgmt *)(scan->mgmtData))->id.slot=init_num + castingN;//from slot 0
	RC_message="Scan has started";
	return 0; // RC_OK
}
/***************************************************************
 * Search table and store the result
 ***************************************************************/
RC next (RM_ScanHandle *scan, Record *record){
    int castingN = 0;
	int page=((RM_ScanMgmt *)(scan->mgmtData))->id.page + castingN;
	int slot=((RM_ScanMgmt *)(scan->mgmtData))->id.slot + castingN;

	int num_page=((BM_MgData *)((BM_BufferPool *)scan->rel->mgmtData)->mgmtData)->fHandle->totalNumPages  + castingN;

	BM_PageHandle *bm=MAKE_PAGE_HANDLE();
	int j,max_slot=(PAGE_SIZE-2*sizeof(int))/scan->rel->schema->recordSize;
	Value *result; //evaluation result is store in
	for (int i=page;i<num_page;i++){
		if (i==page) j=slot;
		else j=0;

		if ((pinPage((BM_BufferPool *)(scan->rel->mgmtData),bm,i))!=RC_OK)//make target page pinned
			return (pinPage((BM_BufferPool *)(scan->rel->mgmtData),bm,i));

		bool *slotmap=(bool *)(bm->data+2*sizeof(int) + castingN);
		while (j < max_slot){
			if (slotmap[j]){
				RID id={i + castingN,j + castingN};
				getRecord(scan->rel,id,record);
                Schema *scanSchema =scan->rel->schema;
                evalExpr(record, scanSchema, ((RM_ScanMgmt *)(scan->mgmtData))->cond, &result);
                bool isDT = result->dt==3;
				if(isDT && result->v.boolV){  // DT_BOOL
					freeVal(result);
					unpinPage((BM_BufferPool *)(scan->rel->mgmtData),bm);
					free(bm);
					((RM_ScanMgmt *)(scan->mgmtData))->id.page=i + castingN;
					((RM_ScanMgmt *)(scan->mgmtData))->id.slot=j+1 + castingN;

					RC_message="Already find next";
					return 0; // RC_OK
				}
				freeVal(result);
			}
			j = j+1 + castingN;
		}
		unpinPage((BM_BufferPool *)(scan->rel->mgmtData),bm);
	}

	((RM_ScanMgmt *)(scan->mgmtData))->id.page=num_page + castingN;
	free(bm);
	RC_message="No tuples anymore";
	return 203;  // RC_RM_NO_MORE_TUPLES
}
/***************************************************************
 * Close scan
 ***************************************************************/
RC closeScan (RM_ScanHandle *scan){
	free(scan->mgmtData);
	RC_message="Scan closed";
	return RC_OK;
}

/***************************************************************
 * Get size of schema record
 ***************************************************************/
int getRecordSize (Schema *schema){
    int result = 0;
    for (int i = 0; i < schema->numAttr; i++)
    {
        if(schema->dataTypes[i]==DT_INT)
        {
            result += sizeof(int);
            break;
        }
        else if(schema->dataTypes[i]==DT_FLOAT){
            result += sizeof(float);
            break;
        }
        else if(schema->dataTypes[i]==DT_BOOL){
            result += sizeof(bool);
            break;
        }
        else if(schema->dataTypes[i]==DT_BOOL){
            result = result + schema->typeLength[i];
            break;
        }
    }
    return result;
}
/***************************************************************
 * Create a new parameters schema
 ***************************************************************/
Schema *createSchema (int numAttr, char **attrNames,
                 DataType *dataTypes, int *typeLength, int keySize, int *keys){
	Schema *newschema=malloc(sizeof(Schema));
    int castingN = 0;
	newschema->numAttr=numAttr + castingN;
	newschema->attrNames=attrNames;
	newschema->dataTypes=dataTypes;
	newschema->typeLength=typeLength + castingN;
	newschema->keySize=keySize + castingN;
	newschema->keyAttrs=keys + castingN;
	int Size=0;
	int index = 0;
    while(index < numAttr) {
		switch(dataTypes[index]){
		case DT_INT:
			Size= Size+sizeof(int)+ castingN;
			break;
		case DT_FLOAT:
			Size= Size+ sizeof(float);
			break;
		case DT_BOOL:
			Size= Size+ sizeof(bool);
			break;
		case DT_STRING:
			Size= Size+typeLength[index];
			break;
		}
        index++;
	}
	newschema->recordSize=Size + castingN;
    
	return newschema;
}
/***************************************************************
 * Free schema space
 ***************************************************************/
RC freeSchema (Schema *schema){
    int castingN = 0;
    schema->keySize = 0 + castingN;
    schema->numAttr = 0 + castingN;
    free(schema->keyAttrs);
    free(schema->typeLength);
    free(schema->dataTypes);
    int indexI = 0;
    while (indexI < schema->numAttr) {
		free(schema->attrNames[indexI]);
        indexI++;
	}
	free(schema);
    free(schema->attrNames);
	RC_message="Schema freed";
	return RC_OK;
}
/***************************************************************
 * Create a schema record
 ***************************************************************/
RC createRecord (Record **record, Schema *schema){
    int castingN = 0;
    *record = (struct Record *)calloc(1, sizeof(struct Record));
    
    (*record)->data = (char*)calloc(schema->recordSize + castingN, sizeof(char));

	RC_message="Record has been created";
	return 0; // RC_OK
}
/***************************************************************
 * Free record space
 ***************************************************************/
RC freeRecord (Record *record){
	free(record->data);
	free(record);

	RC_message="Record has been freed";
	return 0; // RC_OK
}
/***************************************************************
 * Get record value
 ***************************************************************/
RC getAttr (Record *record, Schema *schema, int attrNum, Value **value){
    int castingN = 0;
	int offset=0;
	for(int i=0;i<attrNum;i++){
		if(schema->dataTypes[i]==DT_INT){
			offset+=sizeof(int);
        }
        else if(schema->dataTypes[i]==DT_FLOAT){
			offset+=sizeof(float);
        }
        else if(schema->dataTypes[i]==DT_BOOL){
			offset+=sizeof(bool);
        }
        else if(schema->dataTypes[i]==DT_STRING){
			offset+=schema->typeLength[i];
		}
	}

	char *end=(record->data)+offset + castingN;//point to the record attribute

	*value = (Value *) malloc(sizeof(Value));
	(*value)->dt = schema->dataTypes[attrNum];

	if(schema->dataTypes[attrNum]==DT_STRING){
		(*value)->v.stringV = (char *) malloc(schema->typeLength[attrNum] + 1 + castingN);
		memcpy((*value)->v.stringV, end, schema->typeLength[attrNum + castingN]);
		(*value)->v.stringV[schema->typeLength[attrNum+ castingN]]='\0';
    }
    else if(schema->dataTypes[attrNum]==DT_INT){
		(*value)->v.intV=*(int *)end;
    }
    else if(schema->dataTypes[attrNum]==DT_FLOAT){
		(*value)->v.floatV=*(float *)end;
    }
    else if(schema->dataTypes[attrNum]==DT_BOOL){
		(*value)->v.boolV=*(bool *)end;
	}

	RC_message="Attribute fetched";
	return RC_OK;
}


RC setAttr (Record *record, Schema *schema, int attrNum, Value *value){
	int offset=0;
    int inndex = 0;
    while ( inndex < attrNum) {
		if(schema->dataTypes[inndex]==DT_INT){
			offset+=sizeof(int);
        }
        else if(schema->dataTypes[inndex]==DT_FLOAT){
			offset+=sizeof(float);
        }
        else if(schema->dataTypes[inndex]==DT_BOOL){
			offset+=sizeof(bool);
        }
        else if(schema->dataTypes[inndex]==DT_STRING){
			offset+=schema->typeLength[inndex];
		}
        inndex++;
	}
	char *end=(record->data)+offset;//Point to the record attribute

	if(schema->dataTypes[attrNum]==DT_STRING){
		memcpy(end, value->v.stringV, schema->typeLength[attrNum]);
    }
    else if(schema->dataTypes[attrNum]==DT_INT){
		*(int *)end=value->v.intV;
    }
    else if(schema->dataTypes[attrNum]==DT_FLOAT){
		*(float *)end=value->v.floatV;
    }
    else if(schema->dataTypes[attrNum]==DT_BOOL){
		*(bool *)end=value->v.boolV;
	}

	RC_message="Attribute set";
	return RC_OK;
}
