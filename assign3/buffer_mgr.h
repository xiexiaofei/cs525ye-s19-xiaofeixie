#ifndef BUFFER_MANAGER_H
#define BUFFER_MANAGER_H

// Include return codes and methods for logging errors
#include "dberror.h"
#include "storage_mgr.h"

// Include bool DT
#include "dt.h"

// Replacement Strategies
typedef enum ReplacementStrategy {
    RS_FIFO = 0,
    RS_LRU = 1,
    RS_CLOCK = 2,
    RS_LFU = 3,
    RS_LRU_K = 4
} ReplacementStrategy;

// Data Types and Structures
typedef int PageNumber;
#define NO_PAGE -1

typedef struct BM_BufferPool {
    char *pageFile;
    int numPages;
    ReplacementStrategy strategy;
    void *mgmtData; // use this one to store the bookkeeping info your buffer
    // manager needs for a buffer pool
} BM_BufferPool;

typedef struct BM_PageHandle {
    PageNumber pageNum;
    char *data;
} BM_PageHandle;

typedef struct BM_RepHandle{
    int cur;
    int size;
    int compacity;
    void *stratData;
    void *dataStruct;
}BM_RepHandle;

typedef struct BM_PageFrame{
    int compacity;
    int index;
    PageNumber pageNum;
    bool dirty;
    int fixCount;
    char data[PAGE_SIZE];
}BM_PageFrame;

typedef struct BM_MgData{
    SM_FileHandle *fHandle;
    BM_RepHandle *rep;
    BM_PageFrame *pageFrame;
    int size;
    int index;
    int *pageMap;
    int sizeOfMap;
    int readIO;
    int writeIO;
}BM_MgData;

typedef struct ListEntry{
    int index;
    int compacity;
    struct ListEntry *p;
    struct ListEntry *cur;
    struct ListEntry *n;
}ListEntry;

typedef struct IntList{
    int size;
    ListEntry *h;
    ListEntry *cur;
    ListEntry *t;
}IntList;

typedef struct LRU{
    int size;
    int count;
    int reSizeFactor;
    IntList *frameList;
    int compacity;
    ListEntry *frameMap;
}LRU;


// convenience macros
#define MAKE_POOL()                    \
((BM_BufferPool *) malloc (sizeof(BM_BufferPool)))

#define MAKE_PAGE_HANDLE()                \
((BM_PageHandle *) malloc (sizeof(BM_PageHandle)))

RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName,
                  const int numPages, ReplacementStrategy strategy,
                  void *stratData);
RC shutdownBufferPool(BM_BufferPool *const bm);
RC forceFlushPool(BM_BufferPool *const bm);

// Buffer Manager Interface Access Pages
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page);
RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page);
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page);
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page,
            const PageNumber pageNum);

// Statistics Interface
PageNumber *getFrameContents (BM_BufferPool *const bm);
bool *getDirtyFlags (BM_BufferPool *const bm);
int *getFixCounts (BM_BufferPool *const bm);
int getNumReadIO (BM_BufferPool *const bm);
int getNumWriteIO (BM_BufferPool *const bm);

#endif

