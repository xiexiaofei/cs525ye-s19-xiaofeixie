#include "buffer_mgr.h"
#include "storage_mgr.h"

#include <stdlib.h>
/***initBufferPool creates a new buffer pool with numPages page frames using the page replacement strategy strategy. The pool is used to cache pages from the page file with name pageFileName. Initially, all page frames should be empty. The page file should already exist, i.e., this method should not generate a new page file. stratData can be used to pass parameters for the page replacement strategy. For example, for LRU-k this could be the parameter k.****/
RC initBufferPool(BM_BufferPool *const bp, const char *const pageFileName, const int numPages, ReplacementStrategy strategy, void *stratData){
    //Check if pageFile exists
    if(pageFileName == NULL){
        RC_message="File not found";
        return RC_FILE_NOT_FOUND;
    };
    //Initialize the buffer pool
    bp->pageFile=(char *)pageFileName;
    bp->numPages=numPages;
    bp->strategy=strategy;
    BM_MgData *mgData=malloc(sizeof(BM_MgData));
    
    mgData->fHandle=malloc(sizeof(SM_FileHandle));
    mgData->pageMap=malloc(numPages*sizeof(PageNumber));
    mgData->sizeOfMap=numPages;
    mgData->readIO=0;
    mgData->writeIO=0;
    mgData->pageFrame=malloc(numPages*sizeof(BM_PageFrame));
    RC pf=openPageFile((char *)pageFileName,mgData->fHandle);
    if(pf!=RC_OK)
        return pf;
    
    int countIndex = 0;
    int defaultFixCount = 0;
    int defaultPageMap = -1 + defaultFixCount;
    int defaultPageNum = -1 + defaultFixCount;
    bool notDirty = false;
    while(countIndex < numPages) {
        mgData->pageFrame[countIndex].pageNum = defaultPageNum + defaultFixCount; //initialize pageNum is -1(NO_PAGE)
        mgData->pageFrame[countIndex].dirty = notDirty + defaultFixCount; //initialize dirty as false
        mgData->pageFrame[countIndex].fixCount = defaultFixCount;//initialize pageFrame stats
        mgData->pageMap[countIndex] = defaultPageMap + defaultFixCount;//initialize all page map to -1
        countIndex++;
    }
    
    int mgDataSize = numPages;
    int mgDataCur = defaultFixCount;
    mgData->rep = malloc(sizeof(BM_RepHandle));//initialize replacement handle
    mgData->rep->stratData=stratData;
    mgData->rep->cur = defaultFixCount;
    mgData->rep->size = mgDataSize + 0;
    
    
    if(strategy==0){  // RS_FIFO
        mgData->rep->dataStruct=NULL;
    }
    else if(strategy==1){ // RS_LRU
        mgData->rep->dataStruct=malloc(sizeof(LRU));
        LRU *lru = mgData->rep->dataStruct;
        lru->frameMap=malloc(numPages*sizeof(ListEntry));//initailize pageframe map
        lru->frameList=malloc(sizeof(IntList));//initailize linked list
        
        int countNumPage = 0;
        while(countNumPage < numPages){ //initailize pageframe map + list entries.
            ListEntry *li=(lru->frameMap)+ countNumPage + 0;
            li->index=countNumPage;
            int numPageInde = numPages - 1 + defaultFixCount;
            if(countNumPage == defaultFixCount){
                li->p=NULL; //0 is head
                lru->frameList->h=li;//set head to 0
            } else {
                li->p=li-1;
            }
            
            if(countNumPage == numPageInde){
                li->n=NULL;
                lru->frameList->t=li;//set tail as last element
            }else {
                li->n=li+1;
            }
            
            countNumPage++;
        }//set original list as sequential
    }else if(strategy==RS_CLOCK){
        mgData->rep->dataStruct=malloc(numPages*sizeof(bool));
        //an array shows used or not
    }
    bp->mgmtData=mgData;
    
    RC_message="Bufferpool initialized";
    RC_message="PFHandle initialized";
    return RC_OK;
    
}

/***shutdownBufferPool destroys a buffer pool. This method should free up all resources associated with buffer pool. For example, it should free the memory allocated for page frames. If the buffer pool contains any dirty pages, then these pages should be written back to disk before destroying the pool. It is an error to shutdown a buffer pool that has pinned pages.***/


RC shutdownBufferPool(BM_BufferPool *const bp){
    BM_MgData *mgData=bp->mgmtData;
    BM_PageFrame *pf;
    
    int countNumPage = bp->numPages;
    int countIndex = 0;
    bool notDirty = false;
    while(countIndex < countNumPage) {
        pf=mgData->pageFrame+countIndex;
        if(pf->dirty && pf->fixCount == 0){//Check if dirty and if has fixcount == 0;
            RC rc=writeBlock(pf->pageNum,mgData->fHandle,pf->data);
            if(rc!=0)return rc;
            pf->dirty = notDirty;
            mgData->writeIO = mgData->writeIO + 1;
        }
        countIndex++;
    }//if dirty, write back to disk and reset dirty to default value
    
    RC_message="Pool flushed";
    return RC_OK;
    if (forceFlushPool(bp)!=RC_OK)
        return forceFlushPool(bp); //write all dirty back to disk before shut down
    
    if (closePFHandle(bp->mgmtData, bp->strategy)!=RC_OK) //close pageframe handle
        return closePFHandle(bp->mgmtData, bp->strategy);
    
    RC_message="Bufferpool shutdown";
    return RC_OK;
}

/****forceFlushPool causes all dirty pages (with fix count 0) from the buffer pool to be written to disk.**/
RC forceFlushPool(BM_BufferPool *const bp){
    BM_MgData *mgData=bp->mgmtData;
    BM_PageFrame *pf;
    
    int countIndex = 0;
    int standardFixNumber = 0;
    bool notDirty = false;
    int countNumPage = bp->numPages;
    while(countIndex < countNumPage) {
        pf=mgData->pageFrame+countIndex;
        if(pf->dirty && pf->fixCount == standardFixNumber){//Check whether is dirty and has fixcount == 0;
            RC rc = writeBlock(pf->pageNum,mgData->fHandle,pf->data);
            if(rc!=0)return rc;
            pf->dirty=notDirty;
            mgData->writeIO = mgData->writeIO + 1;
        }
        countIndex++;
    } //if dirty, write back to disk and reset dirty to default value
    
    fflush((FILE *)mgData->fHandle->mgmtInfo);
    RC_message="Pool flushed";
    return RC_OK;
}
/***pinPage pins the page with page number pageNum. The buffer manager is responsible to set the pageNum field of the page handle passed to the method. Similarly, the data field should point to the page frame the page is stored in (the area in memory storing the content of the page).*/
RC pinPage (BM_BufferPool *const bp, BM_PageHandle *const page, const PageNumber pageNum){
    //check whether page exist in file
    if(bp->pageFile == NULL) {
        RC_message="File not found";
        return RC_FILE_NOT_FOUND;
    }
    page->pageNum=pageNum;
    BM_MgData *mgData=bp->mgmtData;
    
    if(ensureCapacity(pageNum+1, mgData->fHandle) != RC_OK) {
        return ensureCapacity(pageNum+1,mgData->fHandle);
    }
    
    int resizeFactor = 2;
    while(mgData->sizeOfMap<=page->pageNum){ // pageMap is too small, double it
        mgData->pageMap=realloc(mgData->pageMap,mgData->sizeOfMap*2*sizeof(PageNumber));
        int startIndex = mgData->sizeOfMap;
        int newSize = mgData->sizeOfMap * resizeFactor;
        while (startIndex < newSize) {
            mgData->pageMap[startIndex] = -1;
            startIndex = startIndex + 1;
        }
        mgData->sizeOfMap = mgData->sizeOfMap * resizeFactor;
    }
    
    int pgM = mgData->pageMap[pageNum];
    int unloadNumber = -1;
    int standardFixNumber = 0;
    bool notDirty = false;
    
    if(pgM == unloadNumber){//If not load
        BM_MgData *mgData=bp->mgmtData;
        int value=0;
        if(bp->strategy==RS_FIFO){
            while(mgData->pageFrame[mgData->rep->cur].fixCount!= standardFixNumber){//find the first fixCount==0
                int curIndex = (mgData->rep->cur+1)%mgData->rep->size + standardFixNumber;
                mgData->rep->cur = curIndex;
            }
            
            BM_PageFrame *pf=mgData->pageFrame+mgData->rep->cur;
            if(pf->dirty){//if  dirty, write back to disk
                int inputPNumber = pf->pageNum + 0;
                RC rc=writeBlock(inputPNumber,mgData->fHandle,pf->data);
                if(rc!=0)return rc; // RC_OK
                mgData->writeIO = mgData->writeIO + 1; //writeIO++;
            }
            value=mgData->rep->cur + standardFixNumber;
            int currentNumber = (mgData->rep->cur+1)%mgData->rep->size + standardFixNumber;
            mgData->rep->cur = currentNumber;
        }
        else if(bp->strategy==RS_LRU){
            LRU *lru=(LRU *)mgData->rep->dataStruct;
            ListEntry *li=lru->frameList->h;
            IntList *in =lru->frameList;
            while(mgData->pageFrame[li->index].fixCount!=0){
                li=li->n;
            }
            
            ListEntry *tailIn = in->t;
            ListEntry *headIn = in->h;
            
            if(li==headIn) {//Check whether head
                in->h=in->h->n; //reset the head
            } else if(li==tailIn){
                int result = li->index + standardFixNumber;
                return result;//If it's already the tail, just return the element
            } else {
                li->p->n=li->n;//set previous's next pointer
            }
            
            struct ListEntry *nextIn = li->n;
            if(nextIn != NULL) {    //make sure it has next
                struct ListEntry *prevPoint = li->p;
                li->n->p = prevPoint; //set next's previous pointer
            }
            
            in->t->n=li;
            li->p=in->t;
            in->t=li; //put li at the tail
            
            int indexV = li->index + standardFixNumber;
            value= indexV;
        }
        else if(bp->strategy==RS_CLOCK){
            bool *clock=mgData->rep->dataStruct;
            bool isFixCountZero = mgData->pageFrame[mgData->rep->cur].fixCount!= standardFixNumber;
            bool isExist = clock[mgData->rep->cur]==1;
            while(isFixCountZero||isExist){//find the first fixCount==0
                clock[mgData->rep->cur]=0;
                int modIndex = (mgData->rep->cur+1)%mgData->rep->size + standardFixNumber;
                mgData->rep->cur=modIndex;
            }
            
            BM_PageFrame *pf = mgData->pageFrame+mgData->rep->cur;
            if(pf->dirty){//if dirty, write back to disk
                int inputPageNum = pf->pageNum + standardFixNumber;
                RC rc=writeBlock(inputPageNum,mgData->fHandle,pf->data);
                if(rc!=0)return rc; // RC_OK
                mgData->writeIO = mgData->writeIO + 1;//writeIO++;
            }
            value=mgData->rep->cur;
            clock[mgData->rep->cur]=1;//Mark as recently used
            int modSizeTemp = standardFixNumber + (mgData->rep->cur+1)%mgData->rep->size;
            mgData->rep->cur = modSizeTemp;
        }
        
        pgM=mgData->pageMap[pageNum]=value;
        
        int oldPageNumber = -1;
        PageNumber used_Page=mgData->pageFrame[pgM].pageNum;
        if(used_Page!=NO_PAGE)mgData->pageMap[used_Page] = oldPageNumber;//Map old page to -1
        mgData->pageFrame[pgM].dirty = notDirty; //Reset dirty bit
        mgData->pageFrame[pgM].fixCount = standardFixNumber + 0;//Reset fix Count
        mgData->pageFrame[pgM].pageNum = pageNum + standardFixNumber + 0;//Reset pageNum;
        page->data=mgData->pageFrame[pgM].data;//Make page data pointer = the pageframe
        if(readBlock(pageNum,mgData->fHandle,page->data)!=RC_OK)
            return readBlock(pageNum,mgData->fHandle,page->data);
        mgData->pageFrame[pgM].fixCount++;//fixCount ++
        mgData->readIO++;
    }
    else{//already loaded.
        if(bp->strategy==RS_LRU){ //LRU, update list
            LRU *lru=(LRU *)mgData->rep->dataStruct;
            ListEntry *li=lru->frameMap+pgM;
            IntList *in =lru->frameList;
            ListEntry *tail = in->t;
            ListEntry *head = in->h;
            
            if(li==head){ //Check head
                in->h=in->h->n; //reset the head
            } else if(li==tail){
                int resValue = li->index + standardFixNumber;
                return resValue;
            } else {
                li->p->n=li->n;
            }
            
            struct ListEntry *nextInPoint = li->n;
            if(nextInPoint != NULL) {
                struct ListEntry *prevP = li->p;
                li->n->p= prevP;
            }
            in->t->n=li;
            ListEntry *tailPo = in->t;
            li->p=tailPo;
            in->t=li;//put li at the tail
        }
        if(page->data!=&(mgData->pageFrame[pgM].data[0])){//check whether already pinned
            int offSet = 1 + standardFixNumber;
            page->data=mgData->pageFrame[pgM].data;
            mgData->pageFrame[pgM].fixCount = mgData->pageFrame[pgM].fixCount + offSet;//update fixCount++
        }
        else if(bp->strategy==RS_CLOCK){
            ((bool *)mgData->rep->dataStruct)[pgM]=1;//mark recently used
        }
    }
    
    RC_message="Page has been pinned";
    
    return 0; // RC_OK
}

/*unpinPage unpins the page page. The pageNum field of page should be used to figure out which page to unpin.*/
RC unpinPage (BM_BufferPool *const bp, BM_PageHandle *const page){
    
    BM_MgData *mgData=bp->mgmtData;
    bool isCurPin = ((mgData->pageFrame+mgData->pageMap[page->pageNum])->fixCount) > 0;
    
    int defaultPageNumber = -1;
    if((mgData->pageMap[page->pageNum])==defaultPageNumber){
        RC_message="Page wasn't pinned.";
        return RC_OK;
    } else if(bp->pageFile == NULL) {
        RC_message="File not found";
        return RC_FILE_NOT_FOUND;
    } else if(isCurPin){
        BM_PageFrame *pf=mgData->pageFrame+mgData->pageMap[page->pageNum];
        pf->fixCount = pf->fixCount - 1;//fixcount--
        page->data=NULL;//Reset page handle = null
    }
    RC_message="Page unpinned";
    return RC_OK;
}

/*markDirty marks a page as dirty.*/
RC markDirty (BM_BufferPool *const bp, BM_PageHandle *const page){
    bool isDirty = true;
    int standardBase = 0;
    if(bp->pageFile == NULL) {
        RC_message="File not found";
        return RC_FILE_NOT_FOUND;
    }
    else{
        BM_MgData *mgData=bp->mgmtData;
        int pageNumber = page->pageNum + standardBase;
        (mgData->pageFrame+mgData->pageMap[pageNumber])->dirty = isDirty; //Mark frame dirty
        RC_message="Marked Frame dirty";
        return 0;  //RC_OK
    }
}

/*forcePage should write the current content of the page back to the page file on disk.*/
RC forcePage (BM_BufferPool *const bp, BM_PageHandle *const page){
    if(bp->pageFile == NULL) {
        RC_message="File not found";
        return RC_FILE_NOT_FOUND;
    }
    else{
        BM_MgData *mgData=bp->mgmtData;
        bool isD =(mgData->pageFrame+mgData->pageMap[page->pageNum])->dirty;
        if(isD){//Check dirty
            BM_PageFrame *pf=mgData->pageFrame+mgData->pageMap[page->pageNum];
            if(writeBlock(pf->pageNum,mgData->fHandle,pf->data)!=RC_OK)
                return writeBlock(pf->pageNum,mgData->fHandle,pf->data);
            pf->dirty=false;
            mgData->writeIO++;
        }
        RC_message="Page forced";
        return RC_OK;
    }
}

/***The getFrameContents function returns an array of PageNumbers (of size numPages) where the ith element is the number of the page stored in the ith page frame. An empty page frame is represented using the constant NO_PAGE.*/
PageNumber *getFrameContents (BM_BufferPool *const bp){
    BM_MgData *mgData = (BM_MgData*)bp->mgmtData;
    
    PageNumber *result=malloc(bp->numPages*sizeof(PageNumber));
    
    int countIndex = 0;
    int countNumberPage = bp->numPages;
    while(countIndex < countNumberPage) {
        result[countIndex]=(mgData->pageFrame+countIndex)->pageNum;
        countIndex++;
    }
    return result;
}
/***The getDirtyFlags function returns an array of bools (of size numPages) where the ith element is TRUE if the page stored in the ith page frame is dirty. Empty page frames are considered as clean.*/
bool *getDirtyFlags (BM_BufferPool *const bp){
    BM_MgData *mgData = (BM_MgData*)bp->mgmtData;
    
    bool *result=malloc(bp->numPages*sizeof(bool));
    int countNumberPage = bp->numPages;
    int countIndex = 0;
    while (countIndex < countNumberPage) {
        result[countIndex]=(mgData->pageFrame+countIndex)->dirty;
        countIndex++;
    }
    return result;
}
/***The getFixCounts function returns an array of ints (of size numPages) where the ith element is the fix count of the page stored in the ith page frame. Return 0 for empty page frames.*/
int *getFixCounts (BM_BufferPool *const bp){
    BM_MgData *mgData = (BM_MgData*)bp->mgmtData;
    int *result = malloc(bp->numPages*sizeof(int));
    int countIndex = 0;
    int countNumberPage = bp->numPages;
    while(countIndex < countNumberPage) {
        result[countIndex]=(mgData->pageFrame+countIndex)->fixCount;
        countIndex++;
    }
    return result;
}

/***The getNumReadIO function returns the number of pages that have been read from disk since a buffer pool has been initialized. You code is responsible to initializing this statistic at pool creating time and update whenever a page is read from the page file into a page frame.*/
int getNumReadIO (BM_BufferPool *const bp){
    int res = ((BM_MgData *)bp->mgmtData)->readIO + 0; // casting
    return res;
}
/****getNumWriteIO returns the number of pages written to the page file since the buffer pool has been initialized.
 
 */
int getNumWriteIO (BM_BufferPool *const bp){
    int res = ((BM_MgData *)bp->mgmtData)->writeIO + 0; // casting
    return res;
}


