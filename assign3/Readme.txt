

-------------------------------Added Structures------------------------------------------------


In this implementation, we have the header as the pre-added extra structures like before and this record-
manager header.
Following are the previous buffer-manager added structure lists:
1. BM_PageFrame is a structure for storing information of a page frame:
typedef struct BM_PageFrame{//structrure for pageframe
	PageNumber pageNum;//the corresponding page number in the file
	bool dirty;//mark if the content is modified
	int fixCount;//mark how many users are using this page
	char data[PAGE_SIZE];//page content
}BM_PageFrame;

2. BM_RepHandle is about replacement handle: the cur is the position for Clock and FIFO, pointing to the page
frame that's going to be replaced next; the size is the total number of page frames in Buffer Manager. 
typedef struct BM_RepHandle{//replacement handle
	int cur,size;
	void *stratData;//passed in from initBufferPool
	void *dataStruct;//structure to help with the strategy
}BM_RepHandle;

3. BM_MgData is use to handle pageframes ot buffer pool:
typedef struct BM_MgData{//structure to handle pageframes
	SM_FileHandle *fHandle;//point to the file handle
	BM_RepHandle *rep;//manages replacement
	BM_PageFrame *pageFrame;//array of pageframes
	int *pageMap;//array of mapping from page number to pageframe number
	int sizeOfMap;//check the size of pageMap
    int readIO;//track number of IOs
    int writeIO;//track number of IOs
}BM_MgData;

4. ListEntry is use to define the link list and list entries like below:
typedef struct ListEntry{
	int index;//the pageframe index
	struct ListEntry *p;
	struct ListEntry *n;
}ListEntry;

5. IntList is use to define the IntList like below:
typedef struct IntList{
	ListEntry *h,*t;//dequeue from head, enqueue from tail
}IntList;

6. LRU is use to define the frame of IntList and ListEntry:
typedef struct LRU{
	IntList *frameList;//a list of page frame numbers
	ListEntry *frameMap;//map from page frame number to ListEntry
}LRU;





-------------------------------Functions Details------------------------------------------------


Functions lists:
1. Table and Record Manager Functions
Similar to previous assignments, there are functions to initialize and shutdown a record manager. Furthermore,
there are functions to create, open and close a table. The getNumTuples() function returns the number of 
tuples.
- 1). initRecordManager():
        Creates a new record manager;
        The int Bcapacity defines the capacity of buffer size;
- 2). shutdownRecordManager():
        It destroys a record manager;
        It should free up all resources associated with buffer pool;        
- 3). createTable():
        It creates a page file name, and fill page 0 with freePage = 1, num_tuples = 0, recordSize from schema.
- 4). openTable():
        It opens the page file with a buffer manager.
- 5). closeTable():
        It shutdown the buffer pool and free the allocated memory of rel's attributes.
- 6). deleteTable():
        It destroys the page file.
- 7). getNumTuples():
        Returns the number of tuples.

2. Record Functions
These functions are used to retrieve a record with a certain RID, to delete a record with a certain RID, to insert
 a new record,and to update an existing record with new values. When a new record is inserted the record 
manager should assign an RID to this record and update the record parameter passed to insertRecord.
- 1). insertRecord():
        It find the freePage and insert the record in freeslot;
- 2). deleteRecord():
        It find the page and check the slotmap, mark the record deleted by changing correspoding slotmap from
         true to false;
        Update current slot as free, if the page was previously full, update this page as free again.
- 3). updateRecord():
        Go to the correspoding place and copy the record to file.
- 4). getRecord():
        Go to the correspoding place and check the slotmap is true, copy the information to record.

3. Scan Functions
A client can initiate a scan to retrieve all tuples from a table that fulfill a certain condition. Starting a scan
 initializes the RM_ScanHandle data structure passed as an argument to startScan. Afterwards, calls to the
 next method should return the next tuple that fulfills the scan condition. If NULL is passed as a scan condition,
 then all tuples of the table should be returned.
- 1). startScan():
        Allocating memory for scan to stores the page and slot information;
        It starts from page 1 slot 0.
- 2). next():
        Go through pages and records one by one;
- 3). closeScan():
        Free scanned data.

4. Schema Functions
These helper functions are used to return the size in bytes of records for a given schema and create a 
 new schema.
- 1). getRecordSize():
        Return the schema-> recordSize.
- 2). Schema *createSchema():
        Allocate memory for schema;
        Set each attributes as corresponding input;
        Compute the recordSize.
- 3). freeSchema():
        Free the memory in schema.

5. Attribute Functions
These functions are used to get or set the attribute values of a record and create a new record for a given 
  schema. Creating a new record should allocate enough memory to the data field to hold the binary 
  representations for all attributes of this record as determined by the schema.
- 1). createRecord():
        Allocate memory for new record.
- 2). freeRecord():
        Free the records memory.
- 3). getAttr():
        Return the attrNum-th attribute of record and store it.
- 4). setAttr():
        Store the input to the attrNum-th attribute of record.